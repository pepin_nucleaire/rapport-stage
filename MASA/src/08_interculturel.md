# Rapport interculturel

## L’Espagne

L'Espagne est un pays d'Europe du sud qui occupe la majeure partie de la péninsule ibérique.

Elle est bordée par la mer Méditerranée, l'océan Atlantique, les Pyrénnées, la France et le Portugal. De plus, on peut ajouter des îles comme les Baléares en Méditerranée ou les Canaries en Atlantique. Sa superficie est de 504 030 km carrés, ce qui fait le troisième pays de l'Union Européenne en superficie.

Le paysage espagnol est marqué par différents types de climats. Semi aride dans le sud-est, océanique dans le nord et méditerrénaen dans le reste du pays. Ainsi cette variñetiñe de climat fait que la population espagnole se situe principalement sur les côtes et autour de la capitale.

La langue officielle est le castillan, mais dans quelques communautés autonomes, une langue régionale peut exister et être officielle comme le basque ou le galicien.

## La Rioja

La Rioja est la plus petite communauté autonome d'Espagne. Elle se situe sous le pays Basque au nord du pays.

![Localisation de la rioja](img/rioja.png)

La culture au sein de la communauté est très riche. En effet, c'est un lieu de passage du chemin de Saint Jacques de Compostelle par Logroño et Nájera, mais également le lieu de naissance de la langue castillane où on retrouve les premières traces écrites au monastère de Suso.

On ne peut pas parler de La Rioja sans parler de son vin. En effet sa viticulture est ce qui a fait connaitre le vin espagnole au monde entier. 



## Logroño

Avec ses 150 000 habitants, Logroño est la capitale de La Rioja. Elle se trouve sur la rivière de l’Ebre et se fut une ville au croisement entre plusieurs régions importantes de l’Espagne (voir Figure 12 Logroño). 

![Logroño](img/logrono.jpg)

Comme dans toute la région, la ville est marquée par le vin. Il y a, par exemple, tous les ans plusieurs fêtes organisées autour du vin comme la San Mateo (la fête des vendanges), célébrée en septembre pendant une semaine. Durant cette semaine de fête, un grand nombre d’entreprises locales ferment. 
De plus, Logroño a un patrimoine religieux très important avec ses trois sanctuaires qui s’élancent vers le haut. Tout d’abord, la cathédrale baroque Santa Maria la Redonda située dans le vieux quartier et construite au XVème et XVIIIème siècle. Ensuite L’église Santa Maria del Palacio, église commencée dans un style roman mais les voutes sont gothiques. Et pour finir, l’église San Bartolomé qui constitue le plus bel exemple de sculpture monumentale de la Rioja.

## La gastronomie

Vivre à Logroño impose un passage obligé par la Calle Laurel pour manger ses fameuses tapas ou pinchos. La ville a été élue capitale gastronomique. Le nombre de bar dans le centre ville est vaste et la tradition est de manger une spécialité de bar puis de changer d'endroit pour manger une autre pincho. 

L'ambiance dans la ville est très familiale. Il n'est pas rare de voir les enfants courir entre les gens au milieu de la Laurel remplie, on croise également toute les générations dans ce point central de la ville où les gens se retrouvent pour partager un moment de dégustation.

![pinchos de logroño](img/pincho.jpg){ height=60% }

## La vie locale

Ce qui est étonnant lorsqu'on arrive depuis un pays comme la France et plus encore anglo-saxon, on remarque tout de suite la différence d'horaire de vie. En effet il n'est pas rare de trouver les rues vides et les boutiques fermées en plein milieu d'après-midi. Les repas se font aux alentours de 22h.

## Visites

Voici les différents endroits que j'ai pu visiter lors de mon séjour en Espagne:

![Carte visite](img/visite.png)

\newpage
