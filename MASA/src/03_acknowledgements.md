# Remerciements {.unnumbered}

<!-- This is for acknowledging all of the people who helped out -->

Je souhaite remercier M Pedro Revuelta, PDG de MASA, ainsi que M Alejandro Jiménez, chef du service de suivi de programme, de m'avoir accepté au sein de l'entreprise.

Je remercie également Caroline Besse qui m'a aidé dans la recherche de stage.

De plus, dans mon travail quotidien, M Cyril Vignes, M Alvaro Najarro, M Igarki San Millan m'ont beaucoup apporté tant au niveau technique pour poursuivre mon stage qu'au niveau des conseils qu'ils ont pu m'apporter.

<!-- Use the \newpage command to force a new page -->

\newpage

\tableofcontents

\newpage