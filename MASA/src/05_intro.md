# Introduction

Dans le cadre de mes études à Sigma Clermont, j'ai eu la chance de pouvoir effectuer une année internationale où l'on effectue deux stages de six mois chacun. Ainsi, j'ai effectué le deuxième semestre de stage à MASA.

Dans ce rapport, je présenterai dans un premier temps  l'entreprise d'accueil ainsi que le travail réalisé au sein de celle-ci. Puis j'établirai un rapport culturel qui décrira les aspects interculturels que j'ai pu remarquer en Espagne.

\newpage


