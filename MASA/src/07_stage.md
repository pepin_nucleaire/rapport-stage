# Mon travail

Lors de mon arrivée à MASA, mon sujet de stage a changé. En effet je devais être au service programmes pour suivre les pièces, mais le travail du stagiaire précédent a fait que le travail demandé était moindre par rapport au besoin.

Ainsi il a été décidé dans les premiers jours que je change de sujet et ainsi je travaillerai avec l'amélioration continue où je developperai un outil pour le service Qualité.

## Besoin

Le besoin exprimé par le service qualité était une amélioration du système de données sur le contrôle des bains de traitement chimique.

Le besoin global est de faciliter l'entrée et le traitement de ces données.

### État pré-stage

Les données concernant le traitement des données étaient contenus dans des fichiers Excels mensualisés. De plus, certaines valeurs étaient dupliquées entre plusieurs fichiers excels et cela obligeait le service qualité à passer du temps pour pouvoir traiter ces données efficacement.

Il existe ainsi plusieurs types de fichiers existants :
* Un fichier par mois sur le contrôle des paramètres des bains
* Un fichier par mois sur le contrôle des essais effectués pour s'assurer de l'efficacité des bains
* Un fichier par paramètre de bain qui traite les données afin de d'effectuer une maitrise statistique des procédés. (SPC)

Un effet de ce traitement est que chaque donnée ne possède pas le même format (entier/flottant/séries de caractères).

### Exigences fonctionnelles

* Créer un logiciel avec une base de donnée centralisée
* Pouvoir entrer les données pour les deux types de contrôles
* Pouvoir visualiser les données entrées
* Faire une SPC
* Créer plusieurs groupes d'utilisateurs ayant différents accès en écriture/lecture des données
* Créer un mailing automatique pour certaines fonctions

## Étude des données

### Organisation des données

Les données sont distribués en deux  fichiers quasi similaires et organisés de la même façon :

* Le type de document peut changer de version qu'on appelle révision
* Le document demande trois signatures pour valider les mesures : le chef du laboratoire, le chef de la qualité, le responsable de l'usine
* Le document est trié unique par mois + année
* Par document, Une liste de bains + ses propriétés
* Par bains, une liste de paramètres ou essais avec ses propriétés
* Par paramètre/essai, une liste de mesure et ses propriétés.
* Pour le document Q206.1, les valeurs sont parfois accompagnés de recharge de bains

Voici le diagramme entité/relation qui en a découlé.

![Diagramme ER](img/ER.png){ height=70% }

### Type de données

Après avoir définie le type des données, j'ai créé le modèle relationnel grâce au logiciel SQL Power Architect, qui permet de modéliser les différentes tables et ainsi de générer le code SQL à destination du serveur SQL de MASA.

![Modèle relationnel](img/modeleR.png)

## Création de l'application

### Apprentissage du C#

Les applications sont créés ici en C# langage de programmation objet créé par Microsoft. Il ressemble au C++ qui a été appris en première année à l'IFMA. De plus, pour faciliter la création des applications, le département utilise le framework DevExpress qui permet de créer des interfaces utilisateurs facilement, permettant de concentrer le développement sur le traitement des données.

Durant ma première semaine, j'ai travaillé sur l'apprentissage du C# ainsi que des possibilités que DevExpress offre à travers la création d'applications simples afin d'essayer de reproduire des cas d'utilisation simples.

### Étude de l'expérience utilisateur

Avec la qualité, nous avons fait une première réunion afin de voir comment l'application fonctionnerait d'un point de vue utilisateur. Ainsi cela a pu dégager une première architecture :

![architecture UI](img/architecture.png)

### Modèle MVC

L'application est construite avec un modèle type Model-View-Controller, standard qui permet de créer différentes classes suivant ce qu'elles font.

Ainsi ce découpage en 3 permet de distinguer :

* Modèle de données *Model*, qui contient la forme des données au sein de l'application et fait appel à la base de données *DB* pour les récupérer de manière brute

* Le traitement des données *Controller* qui récupère les données du *Model* et qui les renvoie sur le *View*

* Interface Utilisateur *View* qui regroupe tout l'affichage ainsi que le placement des données par le *Controller*

![MVC Model](img/MVC.png)

Ainsi les habitudes de création d'applications au sein du service sont de créer deux projets : une bibliothèque regroupant le controleur et le modèle, et l'application en elle-même qui regroupe l'interface utilisateur.

### Diagramme des classes

Voici le diagramme des classes généré par Visual Studio

Pour l'UI :

![Diagramme classe UI](img/ClassUI.png){height=50%}


Pour la bibliothèque de l'application :

![Diagramme classe UI](img/ClassDLL.png)

## L'Application

Voici quelques fonctionnalités développés :

![Accueil](img/lab1.png){ height=50% }

Les pages pour afficher les contrôles de paramètres ou d'essais des bains est la même. Il est possible de filtrer les valeurs affichés par date d'analyse, chaque colonne peut être également filtrée. J'ai choisi d'afficher toutes les valeurs par bains afin d'avoir une vue globale. Un repère de couleur est visible pour voir si l'analyse est conforme ou non.

![Contrôle des paramètres](img/lab2.png){ width=50% } 

![Ajout de valeur d'analyses](img/lab3.png){ width=50% }

Pour afficher le diagramme SPC, on sélectionne le paramètre voulu par bain. De plus, si le bain n'a pas été modifié entre 2 révisions, le diagramme affiche toutes ses valeurs.

![Intégration SPC au sein du logiciel](img/lab4.png){ height=50% }

## Difficultés rencontrées

### Compétences manquantes

La première difficulée de taille a été de s'imprégner de la façon de créer les applications. Car si à Sigma, on nous apprend à faire de l'algorithmique simple en C++ ou de la gestion de base de données dans SQL, combiner ces deux compétences a été un peu difficile à prendre en main.

Pour remédier à cela, j'ai effectué des diagrammes d'UML appris en cours, ce qui permet d'avoir une vision globale de notre système. De plus, j'ai pu aussi pas mal m'inspirer de l'application gérant les non-conformités pour avoir une idée de comment procéder à une action.

### Bugs incongrus

Une multitude de bugs ont été découverts lorsque le logiciel a été testé par le service qualité. En effet, les bugs étaient principalement étaient des cas uniques où l'utilisateur entraient des données dans les formulaires avec un typer erronés (chaine de caractères au lieu de numéro par exemple)

Ainsi à cause de cela, j'ai passé beaucoup d'heures à essayer de repérer ces case frontières et essayer de rendre l'application plus robuste au fur et à mesure.

### Demande d'ajout de fonctions

Après chaque réunion avec la Qualité, on m'a demandé de réaliser d'autres fonctions nouvelles qui reléveraient du fonctionnement du programme. 

Par exemple, après une réunion, certaines exigences sur la modifications de données changent ce qui entraîne un certain temps pour tester les fonctionnalités changées.

### Mise en place du Test Driven Development

Dès le début du projet, j'avais tenté de mettre en place un développement logiciel en écrivant d'abord des tests afin de vérifier que chaque fonctionnalités marchent au moment de la compilation. Malheuresement, ce n'est pas la manière de développer au sein du service, et les tests unitaires sur les interfaces utilisateurs sont plus compliqués à mettre en place.

## Autres travaux

### Bibliothèque SPC

Durant le développement, une demande de la qualité a été de faire des graphiques SPC et d'effectuer les calculs de différentes statistiques telles que le Cpk.

Dans un premier temps, j'ai donc développé une première solution répondant directement au problème.

Puis un besoin s'est créé afin de créer facilement des graphiques SPC pour une autre application pour visualiser la capabilité d'autres process.

La bibliothèque a donc pour fonction de créer les différentes de points et les calculs correspondants afin de les ressortir. Ainsi le développeur indique les graphiques et la liste de valeur à mettre dans le SPC et la bibliothèque traite tout cela.

![SPC Workflow](img/SPC.png)



\newpage
