# Conclusion

Effectuer ce stage à MASA m'a apporter de nombreuses réussites sur le plan professionnel et personnel.

En effet, en découvrant une entreprise de la taille d'une PME, je me suis rendu compte que je voudrai plus tard travailler dans des structures de cette taille car elles permettent plus de flexibilité ainsi que d'engagement humain.
Sur le plan des compétences, j'ai pu affiner mes compétences en programmation et en analyse de processus, ce qui apporte un gain dans ma valise professionnelle.

Sur le plan personnel, j'ai pu créer une multitude d'amitié en Espagne que ce soit dans l'entreprise ou au gré des rencontres que j'ai effectué avec par exemple les Erasmus de l'université de la Rioja.

Également, j'ai pu travailler mon espagnol, qui malgré mon accent français fort, j'ai pu développer une confiance et peut ainsi me débrouiller dans un contexte professionnel.
