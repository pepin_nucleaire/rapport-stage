<!-- 
This is the Latex-heavy title page. 
People outside UCL may want to remove the header logo 
and add the centred logo
-->

\begin{titlepage}
    \begin{center}

    % Delete the following line
    % to remove the UCL header logo
    \includegraphics[width=5cm]{img/sigma.png} \includegraphics[width=5cm]{img/masa.png} 
    
        \vspace*{2.5cm}
        
        \huge
        Amélioration continue et création d'une application de suivi pour bains de traitement chimique
        
        \vspace{1.5cm}
        
        \Large
        Julien Muller

        \vspace{1.5cm}

        \normalsize
        Rapport de stage \\
        Année Internationale S2
        
        \vfill
        
        \normalsize
        Tuteur école : Caroline Besse \\
        Tuteur entreprise : Álvaro Najarro

        \vspace{0.8cm}

        % Uncomment the following line
        % to add a centered university logo
        % \includegraphics[width=0.4\textwidth]{style/univ_logo.eps}
        
        \normalsize
        Mecanizaciones Aeronáuticas S.A.\\
        Du 4 Septembre 2017 au 9 Février 2018
        

        % Except where otherwise noted, content in this thesis is licensed under a Creative Commons Attribution 4.0 License (http://creativecommons.org/licenses/by/4.0), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited. Copyright 2015,Tom Pollard.

    \end{center}
\end{titlepage}
