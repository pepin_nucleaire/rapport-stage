# Présentation de MASA

## L’entreprise

MECANIZACIONES AERONÁUTICAS, SA (MASA) est spécialisée dans la fabrication de pièces métalliques finies et équipées pour l’industrie aérospatiale.

Le haut niveau d’exigence au sein des différents services qui composent MASA permet de proposer un processus complet en matière d’élaboration de pièces pour l’industrie aéronautique; de la grande équipe d’ingénieurs chargés de la gestion, de la conception et du calcul à l’achèvement de pièces à travers des procédés de traitements superficiels et de peintures, en passant par la fabrication à l’aide d’une technologie de pointe à grande vitesse. Une grande équipe qui travaille conjointement pour proposer un service intégral et professionnel, basé sur les plus exigeants standards de qualité.

Ses activités principales sont :
* Contrôle numérique : Usinage par contrôle numérique de pièces structurelles. 

![Contrôle numérique](img/prez1.png){ height=25% }

* Achèvement de pièces : Réalisation de travaux d’achèvement de pièces à travers des procédés de traitements superficiels et de peintures.

![Achèvement de pièces](img/prez2.png){ height=25% }

* Assemblage et équipement : Assemblage et équipement de pièces pour l’obtention d’ensembles métalliques aéronautiques.

![Assemblage et équipement](img/prez3.png){ height=25% }

## Chiffre d’affaire

En 2017, le chiffre d'affaires est supérieur à 100 millions d'euros.

## Historique

MASA a été fondée en 1982 en tant que société auxiliaire de l’industrie aéronautique nationale et s’est, depuis lors, développée et agrandie jusqu’à atteindre aujourd’hui une importante reconnaissance internationale.

En 1999, afin de satisfaire la demande de ses clients et de compléter les travaux d’usinage réalisés par MASA, elle a ouvert une usine spécialisée dans la réalisation de travaux d’achèvement de pièces à travers des procédés de traitements superficiels et de peintures.

### Dates Clés

*	Janvier 1, 1940 : Origines : la création de DYE

DYE a été créée à Durango (Biscaye) en 1940, fruit du talent créatif de José Estancona. Son activité principale est la machine-outil : limeuses, aléseuses, tours parallèles, étaux limeurs… Le succès ne se fait pas attendre et consolide la société en tant que leader dans son secteur.

*	Octobre 1, 1980 : Collaboration DYE-CASA

Casa propose à DYE de garder 5 grandes machines en cours de construction pour eux en échange de leur fournir du travail pour ces dernières. Ces machines, qui commencent à être utilisées à Durango en 1981, représentent le point de départ de Masa.

*	Janvier 1, 1983 : Création de MASA

MASA est créée à Logroño (La Rioja) en 1983, et fait ses premiers pas dans le monde de l’aéronautique en se spécialisant dans la fabrication de grandes pièces pour les avions.

*	Novembre 1, 1985 : Nouvelles installations

Création de nouvelles installations dans la Zone Industrielle El Sequero, AGONCILLO (La Rioja).

*	Février 1, 1997 : Agrandissement

Agrandissement d’entrepôts et intégration d’une nouvelle machine de 5 axes GANTRY dotée d’une table de 104 mètres et de 4 ponts de 3 têtes chacun.

* Juillet 1, 1999 : Naissance de TP

Création de TPA (Traitements et Procédés Finaux Aéronautiques), société spécialisée dans la réalisation de travaux de finition de pièces à travers des procédés de traitements superficiels et de peintures.

* Octobre 1, 2001 : Amélioration continue

Implantation d’un centre d’excellence (work shop) spécialisé dans la fabrication de panneaux de fuselage par usinage mécanique (3 machines CN + 3 rouleuses de tôle)

* Juillet 1, 2004 : Certifications

Obtention des certifications ISO 9001 et EN 9100.

* Juillet 1, 2010 : Montage de sous-ensembles

Intégration de sites au sein de MASA pour l’équipement de pièces et le montage de sous-ensembles.

*	Novembre 1, 2011 : Agrandissement de TPA

Agrandissement de TPA avec l’intégration d’un nouveau bâtiment et d’installations pour la réalisation de nouveaux procédés (TSA, particules magnétiques, nouvelles cabines de peinture).

*	Janvier 1, 2015 : Nouveau bâtiment

Construction d’un nouveau bâtiment pour le stockage informatisé de pièces aéronautiques. Création de PREMASA, société spécialisée dans l’achat de matière première aéronautique et de préparation de formats.

*	Janvier 29, 2016 : Expansion

Construction d’un nouveau bâtiment administratif

## Certificats et Qualité

### Certificats
Tous les produits fabriqués par MASA possèdent un certificat de conformité qui garantit leur conformité vis-à-vis des standards de qualité établis par la norme EN9100 / ISO9001, ainsi que des conditions réglementaires et/ou contractuelles de nos clients et des autorités officielles.

### Qualité

La fonction de qualité est en charge du service de garantie de qualité, organe responsable suprême vis-à-vis de la direction de la société. Elle est également basée sur une organisation formelle qui porte sur les engagements établis dans la politique de qualité de MASA. Ainsi, toute l’organisation s’engage à maintenir et contrôler les standards de qualité établis pour chaque produit.

### Contrôle
Mesures de contrôle spécifiques (vérification tridimensionnelle, supportée par Software METROLOG V5) :

* FARO – Capacité – 1 900 mm de diamètre.
* DEA Nº1 : Capacité – 3 000 x 1 200 x 900 mm (longueur, largeur, hauteur).
* DEA Nº1 : Capacité – 2 000 x 800 x 600 mm (longueur, largeur, hauteur).

La fonction de garantie de qualité de MASA a été homologuée par l’industrie aéronautique internationale, conformément à la norme EN9100, étant ainsi apte à réaliser des fonctions concrètes qui exigent une spécialisation et une expérience prouvée dans le secteur, comme :

*	Émission et approbation d’axes de fabrication.
* Émission et approbation d’instructions de contrôle.
*	Approvisionnement et réception technique de matériel aéronautique.

La grande expérience cumulée auprès des principaux clients internationaux du secteur de l’aérospatiale est notre principal atout, ainsi que notre carte de visite pour tous les domaines commerciaux auxquels nous souhaitons participer, et représente notre engagement d’amélioration continue dans l’efficacité de nos procédés.

## Politique de qualité et environnement

La Politique de l’Organisation est de fournir des produits qui respectent les normes spécifiques pour chaque cas, et qui répondent aux exigences légales et réglementaires, ainsi qu’aux exigences de nos clients, devenant ainsi un outil clé menant à une croissance rentable basée sur un engagement d’amélioration continue et de réduction de nos coûts.

La Qualité et la Protection de l’Environnement est une priorité pour la Direction, et il est de la responsabilité de tous les employés de l’organisation de garantir sa mise en œuvre à travers :

* Diffusion de la documentation et des responsabilités associées au SIG (Système Intégré de Gestion)
* Développement de la formation et de la sensibilisation du personnel.
* Respect des procédures et des normes.
* Distribution des indices et des paramètres internes qui mesurent l’efficacité du SIG et garantissent la satisfaction de nos clients.
* Respect de la législation environnementale et autres exigences environnementales orientées vers l’innovation et la durabilité.
* Prévention de la pollution dès son origine, en appliquant de bonnes pratiques environnementales.

La fonction Qualité et Environnement se traduit par un engagement direct vis-à-vis de l’environnement auprès de nos clients ainsi que de notre propre organisation interne en vue de l’efficacité continue et du rendement durable.

## Prix
* Space Best Improvement Award 2017 : Supply Chain Progress towards Aero Space Community Excellence
* Airbus Award 2016 : Details Parts Partners
* Space Best Improvement Award 2015 : Supply Chain Progress towards Aero Space Community Excellence
* Airbus Best Improver Award 2015 : SQIP Materials&Parts
* Premios a la Internacionalización 2015 : Cámara La Rioja
* Airbus Best Improver Award 2014 : SQIP Materials&Parts
* Airbus Best Improver 2013 : SQIP Material&Parts 2015
* Best on Time Delivery supplier 2013 : EADS Sogerma Supplier’s Forum

## Clients et Programmes
### Clients
MASA compte dans ses clients des grands noms de l’aéronautique, comme AIRBUS, BOEING ou encore DASSAULT.

![Clients MASA](img/client.png){height=60%}

### Programmes

| TYPE   | FAMILLE    | TYPE       | FAMILLE     |
| :----: | :--------: | :--------: | :---------: |
| AIRBUS | A-318      | BOEING     | B777        |
| AIRBUS | A-319      | BOEING     | B787-8      |
| AIRBUS | A-320      | BOMBARDIER | Global 7000 |
| AIRBUS | A-320NEO   | DASSAULT   | SMS         |
| AIRBUS | A-321      | DASSAULT   | F7X         |
| AIRBUS | A-330      | EMBRAER    | KC-390      |
| AIRBUS | A-330-200F | EMBRAER    | LEGACY 450  |
| AIRBUS | A-350      | EMBRAER    | LEGACY 550  |
| AIRBUS | A-380      | EMBRAER    | RJ-170      |
| AIRBUS | A400 M     | EMBRAER    | RJ-190      |
| ATR    | ATR        | SOCATA     | TBM

## Département amélioration continue

Le département amélioration continue fait partie du département suivie de programme dirigé par M Alejandro Jiménez.
Il est sous la supervision de M Cyril Vignes.

L'équipe est composée de M Cyril Vignes, M Igarki San Millan, M Alvaro Najarro et M Gabriel Espinosa.

\newpage