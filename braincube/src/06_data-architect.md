# Le métier de Data Architect / Ingénieur structuration de données

Mon stage portait sur un point précis du métier de Data Architect. Lors de mon stage j'ai également appris les rudiments du métier de Data Architect pour bien comprendre le métier et bien intégrer mon sujet dans la façon de travailler du service. 

La définition de la structuration des données est _mettre en place le « jumeau virtuel » du produit, soit rattacher tous les paramètres de fonctionnement de l’usine à chaque unité produite à partir des différentes sources de données disponibles._

Un projet de structuration fait intervenir plusieurs acteurs: 

- un data architect pour la structuration

- un consultant Braincube pour la formation à l'outil auprès du client

- des ingénieurs procédés de l'usine pour la compréhension des lignes de production

- des responsables automatismes pour l'extraction et la compréhension des données machines ou de supervision

- un responsable informatique pour mettre en place la passerelle de données entre l'usine et les serveurs Braincube

Ce projet est composé de plusieurs phases: 

## Kick-off

C'est la première étape et également le début de la relation de confiance entre le client et l'équipe projet Braincube.

Cette étape est parfois précédée d'une venue de l'équipe connectivité qui installe et configure l'envoi de fichier à haute fréquence (de l'ordre de la minute) afin de préparer le travail des clients qui n'ont pas forcément les compétences en informatique.

Le kick off est une session de travail qui dure deux jours. Le kick-off standard est composé de :

* Une présentation du client et de Braincube afin de mieux se connaître mutuellement

* Une visite de l'usine client afin de découvrir les lignes de production sur lesquelles on travaillera et connaîtra les spécificités de celles-ci

* Rappel de sécurité informatique et explication du tunnel sécurisé Braincube

* Identification des données sources, et définition des structures, type et fréquence de téléversement des fichiers dans l'infrastructure

* Création du process mapping pour définir la contextualisation des données

* Bilan des deux jours avec assignation des actions à effectuer jusqu'à la validation de base

Cette étape est essentielle pour garantir que tous les membres du projet soient sur la même longueur d'onde quant aux attentes clients et permettre de faire durer le lien client.

## Structuration

Après le kick-off a lieu la phase de structuration. C'est durant celle-ci que le client et le data architect travaillent pour créer les bases de données définies au Kick-off.

Les tâches classiques du client :

* Mettre en marche l'envoi automatique des fichiers
* Remplir le fichier des DataProperties qui permettent de classifier chaque variable

Les tâches classiques du Data Architect :

* Rassembler les différentes sources de données dans MxBrain
* Créer les bases de données contextualisés : C'est à dire utiliser MxBrain pour réaliser la transformation jusqu'à l'envoi final dans Braincube
* Communiquer avec le client sur les différents points où il peut avoir des points bloquants

## Validation de base

La validation de base est le jalon qui rend compte de la qualité et de la justesse de la base de données.

Cette étape réalisée avec le client consiste à prendre des cas connus sur la ligne de production et valider si le jeu de données est bon ou non.

Ce double regard permet à la fois de corriger des erreurs de structuration, mais aussi de se rendre compte si la captation de données est juste ou non. 
À travers ces échanges, cela peut donner des directions dans l'amélioration continue au sein d'une usine client et ainsi permettre d'augmenter la maturité industrielle du client.

À l'issue de ceci, le client aura confiance dans la base de données et il pourra effectuer ses analyses.



\newpage
