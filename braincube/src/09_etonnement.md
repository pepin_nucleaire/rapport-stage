# Synthèse du stage

## Bilan professionnel

Lors de ce stage, les objectifs initiaux étaient de trouver un outil et de créer un standard de représentation pour les process mappings de Braincube. Ces objectifs ont été atteints avec succès.

J'ai pu trouver le nouvel outil de dessin et créer une bibliothèque de standard pour différentes industries. 
J'ai également confronté mes solutions contre la réalité lors de sessions avec les clients de Braincube. 
Et enfin, j'ai pu déployé avec succès le standard dans le service structuration et inscrit l'intégration dans la feuille de route de Braincube.

De plus, j'ai poussé pour que Drawio puisse être intégré dans l'évolution de la façon de travailler des Data architects et il sera bientôt possible de faire de la structuration de données avec qu'une vue d'ingénieur procédés sans avoir une vue informatique.


## Bilan personel


Mon projet de fin d'études au sein de Braincube m'a apporté énormément d'enseignements que ce soit au niveau technique des traitements de données, ou bien dans mes connaissances de multiples industries.
En effet, depuis le début de ma scolarité à l'école d'ingénieur, tous mes stages étaient au sein de l'industrie aéronautique. De ce fait j'ai très peu connu les problématiques de l'industrie à grande série.

Grâce à Braincube, j'ai pu découvrir différents milieux industriels tels le caoutchouc, le papier et le verre, avec leurs différentes problématiques. Cette diversité a été extrêmement enrichissante pour mon expérience professionnelle.
De plus, j'ai pu découvrir différents niveaux de maturité industrielle en terme de traçabilité et suivi de production, avec des usines quasiment connectés et modernes, ou l'inverse, peu connectés avec des difficultées pour retracer des produits.

\newpage
