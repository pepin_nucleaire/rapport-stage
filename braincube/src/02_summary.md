\pagenumbering{roman}
\setcounter{page}{1}

# Remerciements {.unnumbered}


Je souhaite remercier tout d'abord Émilien Delqueux qui m'a acceuilli dans son équipe et m'a fait confiance durant ce stage effectué au sein de Braincube.

Je souhaite également Marc Davis qui m'a suivi tout au long du stage pour m'aider à avancer sur le projet.

Je remercie Natacha Marcy, Marianne Cluzel et Guillaume Normand pour m'avoir formé et appris les rudiments du métier de Data Architect et qui ont accepté d'utiliser ma solution en test lors de sessions chez les clients.

Un remerciement aussi à Vincent Barjaud qui m'a beaucoup préparé pour se présenter et discuter face aux clients.

Et pour finir tout les data architects qui durant tout le stage ont apporté bonne humeur et entraide tous les jours à Braincube. Ils m'ont aidé à préparer et critiquer mes travaux réalisés lors de ce stage.



\newpage


# Glossaire {.unnumbered}

- Braincube : Entreprise et logiciel de visualisation et d'analyse de Big data.

- Process Mapping : Diagramme de ligne de production où est modélisé la transformation de données Braicube

- Big Data : Ensembles de données devenus si volumineux qu'ils dépassent l'intuition et les capacités humaines d'analyse et même celles des outils informatiques classiques de gestion de base de données ou de l'information

- MxBrain : Logiciel de traitement automatique de données

- ETL : Tunnel de transfert, transformation automatique de données

- csv, xls, txt : Extensions de fichiers de données

- JSON :JavaScript Object Notation, c’est un format de données créer pour stocker les objets en JavaScript

- Braincube IOT : Braincube IOT (Internet Of Things / internet des objets en français), désigne un programme d’extraction des données à partir de base de donnée ou de machines de la chaîne de production




\newpage

\tableofcontents

\newpage

\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}