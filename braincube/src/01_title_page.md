<!-- 
This is the Latex-heavy title page. 
People outside UCL may want to remove the header logo 
and add the centred logo
-->

\begin{titlepage}
    \begin{center}

    % Delete the following line
    % to remove the UCL header logo
    \includegraphics[width=5cm]{img/sigma.png} \includegraphics[width=3cm]{img/braincube.png} 
    
        \vspace*{2.5cm}
        
        \huge
        Création d'un outil de capitalisation de l’intelligence opérationnelle du service structuration afin d'assurer une scalabilité pérenne des activités
        
        \vspace{1.5cm}
        
        \Large
        Julien Muller

        \vspace{1.5cm}

        \normalsize
        Rapport de stage \\
        Projet de fin d'études
        
        \vfill
        
        \normalsize
        Tuteur école : Olivier Devise \\
        Tuteur entreprise : Émilien Delqueux

        \vspace{0.8cm}
        
        \normalsize
        Braincube\\
        Du 19 Mars au 21 Septembre 2018
        

        % Except where otherwise noted, content in this thesis is licensed under a Creative Commons Attribution 4.0 License (http://creativecommons.org/licenses/by/4.0), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited. Copyright 2015,Tom Pollard.

    \end{center}
\end{titlepage}
