# Introduction

Dans le cadre de la fin de cursus à Sigma Clermont, j'ai effectué un projet de fin d'études de six mois au sein de l'entreprise Braincube à Issoire. 

Braincube est une entreprise qui propose une solution big data pour la transformation numérique des entreprises industrielles.

L'objectif de ce stage est de créer un standard du métier de data architect pour réaliser les process mappings. Afin d'arriver à cela, un outil et plusieurs modélisations ont été créés.

Dans ce rapport, je présenterai dans un premier temps l'entreprise d'accueil puis le métier de Data Architect à côté duquel j'ai évolué et enfin le travail que j'ai réalisé pour parvenir à l'objectif fixé.


\newpage



