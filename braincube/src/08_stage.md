# Mon travail

## Les process mappings

### Contexte

Ce stage s'inscrit dans une politique de développement de l'activité à Braincube. En effet, durant le stage, 7 millions d'euros ont été levés auprès d'investisseurs afin de rentrer dans une phase d'hypercroissance où l'activité se multipliera par trois ou quatre. Dans ce contexte, la société fait évoluer ses outils pour pouvoir soutenir cette montée de charge de travail.

Ainsi, mon sujet de stage permet au métier du data architect d'établir un début de standard de structuration spécifiquement sur les process mappings. 

Le fait était que ceux-ci étaient réalisés dans un onglet d'un fichier Excel où les data architects copiaient/collaient des formes préfaites pour pouvoir représenter le process du client. 
f
Cette approche possédait plusieurs défauts :

- peu intuitive, car Excel n'est pas prévu pour faire des diagrammes même s'il en possède la fonction.

- peu pérenne, car les process mappings d'une même ligne de production pouvait avoir des diagrammes différents selon la créativité de son auteur

- les informations contenues dans le document étaient difficilement récupérables

Aussi, ce sujet de process mapping s'intègre dans une future évolution de Braincube qui a vocation à faire évoluer le middleware actuel MX Brain.

### Qu'est-ce qu'un le Process mapping ?

Le process mapping est la première étape de la structuration d'une ligne de production. Il est réalisé lors du kick-off par le data architect et les personnes relevant du process client. 

La réalisation avec le client se fait en deux principales étapes. Tout d'abord, une visite de l'usine et particulièrement de la ligne de production est faite afin d'identifier les principales sources de données que l'on peut avoir et visualiser le process utilisé par le client. 

Les différents types de lignes de production (continue, discontinue, à batch) se modélisent de façon différente et auront un process mapping correspondant.

Dans un deuxième temps, suivant le type de process, nous identifions chaque variable sortie par le client qui peuvent créer au plus près le jumeau numérique du produit, afin d'obtenir un granulométrie des données le plus fin possible. 

En prenant l'exemple d'un papetier et d'une ligne de production de pâte à papier, la modélisation comporte principalement des cuviers, il est nécessaire d'obtenir chaque variable de niveau, de flux entrants ou sortants, voire les concentrations de la pâte. 

Ainsi à partir de chaque élément de modélisation, on définit ensuite un *lag group*. Ce terme définit un décalage temporel dans un process continu. Ainsi cela permet à Braincube de faire une traçabilité d'un produit provenant d'un process continu.


### Caractérisation des besoins

Pour établir des process mappings standards dans le cadre de Braincube, il faut avoir :

- Des modèles de transformations de données standards

- Un logiciel de diagramme simple et efficace à utiliser 

- La possibilité d'avoir des fichiers utilisables informatiquement afin de pouvoir les utiliser dans une future évolution du middleware

- Une application web afin de garantir l'utilisation sur différents systèmes d'ordinateurs et pour faire une intégration à Braincube

De plus des critères de coût étaient présents et il fallait que ce logiciel soit gratuit et utilisable commercialement. Ainsi qu'un critère de temps de développement qui devait être minimisé afin de pouvoir sortir rapidement l'application.

L'utilisation de modèles standards de transformation permettra dans l'expansion de Braincube d'avoir une capitalisation de l'intelligence opérationnelle pour pouvoir être compréhensible de tous data architects. 

Par exemple, avec la création récente d'un service support de structuration, les équipes pourront utiliser le même principe de structuration pour comprendre ce que chaque opération de données représente. 

### Choix de l'outil

Pour choisir l'outil, j'ai d'abord effectué un état de l'art et une veille technologique des logiciels de diagramme existants grâce à des recherches différentes sur Google et Github pour pouvoir trouver une solution répondants aux besoins.

J'ai également fait participer des membres de l'équipe de R&D pour apporter leurs connaissances techniques pour connaître la faisabilité des solutions proposées sur plusieurs critères:

- Utiliser ou non des solutions existantes qui étaient possiblement compatibles avec le besoin de mon projet

- Intégrabilité dans Braincube

- Développement supplémentaire des outils proposés

Ensuite avec deux collègues, nous avons établi une matrice de notation afin de pouvoir comparer objectivement les différents logiciels existants.

Les critères correspondent à diverses fonctionnalités / comportement voulus par les utilisateurs et également des fonctionnalités permettant une intégration facilitée dans Braincube. 
Ces critères ont été pondérés si ils ont été jugés cruciaux pour la mise en place du logiciel. Chaque note est binaire.

De plus, j'ai intégré Excel qui était utilisé avant pour en avoir une bonne comparaison avec une future solution.

Le logiciel choisi a donc été Drawio

![Matrice de choix du logiciel](img/matrice_choix.png)

### Drawio

Drawio est un logiciel écrit en javascript par jGraph basé sur la librairie MxGraph. 

C'est un logiciel simple d'utilisation en drag & drop de formes pouvant être modifiées à volonté et liées par des flèches.

Sa principale force est le fait de pouvoir exporter facilement en XML les diagrammes créés pour pouvoir être importés facilement par un logiciel. 
Étant aussi open source, il est pensé de pouvoir extraire le diagramme en format JSON, format plus souvent utilisée en interne pour le développement d'applications javascript.

D'autre part, la fonctionnalité de pouvoir noter des métadonnées pour chaque élément de process permet au Data Architect de ne pas oublier de variables nécessaires à la création du diagramme.

De plus, on peut créer notre propre forme incluant ses métadonnées dans une bibliothèque afin d'avoir une bibliothèque standard partagée entre chaque Data Architect.

### Définition du standard

Pour définir les standards, je me suis basé sur ce dont un Data Architect a besoin pour créer une base dans Braincube.

D'une part, j'ai passé du temps à regarder les différents process mappings existants, pour agréger des bonnes pratiques et quelques astuces pour un process mapping.

Après avoir analysé avec les data architects, nous avons repéré plusieurs besoins de standardisation:

- Définir un lag group : calcul d'un delta temporel sur un élément de process continu. Généralement calculé avec un débit/volume ou une vitesse/longueur, parfois non calculé car il est défini fixe.

- Faire de la traçabilité

- Représenter la ligne de production afin que le client puisse reconnaître son usine

Ainsi à partir de ces besoins, j'ai créé une bibliothèque de formes répondant au besoin annoncé

![La librairie contenant les éléments de process créés](img/draw/library.png){ width=75% }

Et plus en détail un lag group contenant un cuvier : Dans chaque propriété, le data architect doit ajouter les variables correspondantes aux débits/concentrations/niveau/volume du cuvier afin de pouvoir effectuer une modélisation.

![Lag group contenant un cuvier](img/draw/lag_chest.png){ height=20% }

Ainsi après avoir défini tous ces éléments de process, j'ai défini une base de structure à destination de différentes industries clientes :

- Papier

- Verre

- Extrusion de pneu

Parce que Braincube a une bonne expérience dans la modélisation de ces industries, nous nous sommes rendu compte que beaucoup de process se modélisent de la même façon, moyennant quelques spécificités selon les clients.

Ainsi le data architect se base sur un début de ligne pour ensuite compléter selon chaque client.

![Exemple de process mapping sur la ligne de gateau au chocolat](img/soutenance/chocolate.png)

### Tests auprès du client

Durant les phases de test, j'ai pu tester l'utilisation de mon logiciel dans un contexte réel en accompagnant les data architects auprès des clients.

Dans ce contexte, j'ai pu montrer la force de l'outil aux data architects et assister les data architects dans la réalisation de process mappings. 

J'ai effectué des tests dans différents types d'industries : 

- Verre

- Papier 

- Mélange et extrusion de caoutchouc

Ces tests m'ont permis de faire une détection des différents défauts existants dans la représentation. J'ai ainsi pu les corriger et améliorer la pérennité du standard.
Évidemment, les standards établis jusqu'à présent ne représente qu'un échantillon des clients que j'ai pu visiter. 
Ce standard pourra bien sûr évoluer pour être plus robuste sur d'autres industries.


### Déploiement en production

Le déploiement auprès de tous les utilisateurs est inscrit dans la feuille de route pour une future version de Braincube.
Comme la date d'intégration est dans quelques mois, j'ai proposé au service une solution alternative afin que les data architects puissent utiliser le nouvel outil.

Ainsi avant d'avoir une version finale qui sera une application web sur Braincube, un fonctionnement alternatif a été trouvé.

Pour utiliser Drawio, les data architects téléchargent une version locale de Drawio pour utiliser les fichiers de process mappings sauvegardés sur un répertoire partagé dans notre infrastructure.

Pour pouvoir former les membres du service à l'utilisation de Drawio, j'ai créé de la documentation au sein du wiki interne et j'ai organisé des sessions de formation à l'outil. 
De plus un canal de communication est ouvert au sein de notre outil interne.


## Les méthodes MX

MX Brain est le logiciel développé en interne de transformation des données utilisé par les data architects pour pouvoir alimenter Braincube. Il permet par une suite d'opérations de contextualiser les données.

Une de ces opérations est d'écrire un script en Java pour effectuer des calculs entre colonnes ou d'autres opérations simples qui ne sont pas une opération propre *MxBrain*. 

L'intêrét de ces méthodes est d'accélerer la formation des data architects. 
Le but est de réduire l'aspect de programmation de script au minimum afin de pouvoir concentrer la formation sur la structuration des données plutôt que sur la programmation java.

### Wikistructu et regroupement des méthodes

Le wiki structu est un wiki interne au service permettant de partager toutes les astuces du métier de structuration.

On peut retrouver des informations telles :

* Comment utiliser les opérations élémentaires de MX (Pivot, Mapping, Groupby)

* Des commandes d'administration simple pour les serveurs

* Écriture de scripts pour réceptionner les fichiers clients
 
* Comment écrire les scripts en Java avec différents cas de figure qu'on retrouve souvent

Sur ce dernier point, j'ai compilé une trentaine de méthodes qui réduisent l'écriture de scripts souvent utilisés avec des copier/coller afin de réduire la complexité des scripts.

Pour réaliser la mise en commun des méthodes sur les différentes machines virtuelles MX, un serveur commun opère une synchronisation des différentes méthodes sur tous les MX clients. 

### Tests unitaires

Pour réduire les erreurs, j'ai également implanté des tests unitaires sur ces méthodes. 

La définition d'un test unitaire est : _une procédure permettant de vérifier le bon fonctionnement d'une partie précise d'un logiciel ou d'une portion d'un programme_

Une table test a été ainsi créée et dans celle-ci chaque méthode est testée afin de voir si elles fonctionnent dans tous les cas imaginables.

Ainsi dans l'éventualité de l'introduction d'un bug, cette table annoncera qu'une méthode ne marche pas comme prévu. 

\newpage
