## Models

### Vehicle

The vehicle used in the optimisation is the FSPL described before [§].
A mass breakdown has been done by a function that is compute the mass of the rocket. A new surface is inputed and scaling component (b), and we get a new mass based on reference surface and mass.

$m_{new} = m_{ref} * (\frac{S_{new}}{S_{ref}})$

### Aerodynamics

Aerodynamics data were provided from previous analysis by the aerodynamicists.

They modelled coefficient of lift CL and drag CD using 
 
$C_L = C_N * cos(\alpha) - C_{D0} * sin(\alpha)$ 

$C_D = C_N * sin(\alpha) - C_{D0} * cos(\alpha)$ 

where CD0 is the the drag coefficient at zero incidence and CN the normal force coefficient.
This method is valid for small angle of attack 
Coefficients CN and Cd0 are based on different theories for each :ach number range including subsonic and hypersonic range.

![Altitude dependence of drag coefficient at zero incidence](img/drag.png){ width=70% }

At the end, aerodynamic data provided are the coefficients  and  within a range from -5 to 40 degrees angle of attack and a range from 0 to  30 Mach.

![Lift and drag coefficients of the vehicle configuration as a function of Mach number and angle of attack](img/drag2.png)


### Aerothermodynamics

The aerothermodynamic model is used to compute heat fluxes, heat loads and radiative equilibrium temperature. The model was created by Fluid Gravity Engineering.

The heat fluxes are calculated in 5 different locations.

![Locations where heat fluxes are computed. X34 geometry similar to FSPL](img/aeroth_point.png){ width=70% }

Here, only heat fluxes are computed inside the simulation because of it being computationally faster.

For more insight in the equations used in the model, see the paper.

### Environment

Earth is modelled as an oblate spheroid based on the WSG-84 model.
Atmospheric model is modelled using the International standard atmosphere (ISA model).

### Propulsion

Engines are modelled using Tsiolkovsky rocket equations with configurable inputs specifying the specific impulse and thrust in vacuum.

The two main stages engines uses a liquid oxygen/kerogen propellant based on the Yuzhnoye RD-8 rocket engines. They have an Isp between 300-400 s. 
Flight dynamics
The rocket is modelled  as a 3 degrees of freedom point mass located at the center of gravity of the vehicle. The state vector is the spherical coordinates  and velocity of the point. 

The trajectory dynamics are controlled by adjusting the thrust vector : throttle, angle of attack and bank angle. The engines have no gimbal, so the control is the attitude of the vehicle.

## Optimisation

### Principia

The optimisation mission is divided in two parts. First, the optimizer minimises the gross takeoff weight (GTOW) in order to minimise the propellant used to achieve orbit in ascent phase. Secondly, for the reentry phase, the vehicle has to maximise the distance reached when gliding. 

The simulation is provided by several phases in which are defined vehicle configuration and dynamics, initial and final state,  an objective and path constraints. Each phases is defined by a different stage or a discontinuity (sub/hypersonic).

Then a direct multi-shooting transcription is made to transform the continuous optimal control problem into a nonlinear programming problem solved with a gradient-based optimisation algorithm.

### Formulation
The problem is formulated as :

![formulation](img/formulation.png){ width=25% }

where $J$ is a function of the state vector, control vector and time; $F$ a set of differential equations describing the dynamics of the system, $g$ a set of inequalities describing path constraints, $\psi$ a set of boundary constraints. 

The mission is divided in several $n_p$  phases each divided in $n_e$ elements for the multiple-shooting approach. In each element there are $n_c$ control nodes. 

Then the trajectory optimisation vector is composed for each segment of :

* control nodes
* time of flight
* initial state and control variable

### Optimisation algorithm

Problem is solved by using fmincon function in MATLAB using sqp algorithm. It is 
After transcribing the problem, multiple first-guess solutions are generated within a search space through Latin Hypercube Sampling. This allow a chance to find the best local minimum of the objective function. 

## Mission

There is 4 phases that define the mission :

* First phase is the first stage ascent with the second stage and the payload. It starts right after the release by the aircraft and ends with the staging.
* Second phase is the ascent to orbit of the second stage. Orbit is defined by a 600km altitude circular orbit
* Third phase is the ballistic phase of the first stage and it ends when it reach the atmosphere dense enough so that the spaceplane can control itself.
* Last phase is the glide to landing

### Ascent phase
The ascent phase is defined by the phase 1 and 2 described above. 

Its defined by the parameters below:

![Conditions table](img/tableau.png)

The objective function here is the minimisation of the gross weight of all stages.

For these phases I played with the optimiser in order to get a good enough result so that i could work on the descent phases.

After many attempts, i have not got better values for the objective function. 
This is mainly due because I took a wrong approach of the problem. My knowledge in optimisation were quite low before the beginning of this internship, and I spend many times and tries in order to understand what the software do and how the computation is done. 

In order to get some data, I tried to compute several times with a different wing area near  the reference one. These were not the most optimised.

![Example of data visualised during an ascent - Ascent phase](img/ascent.png){ height=70% }

### Descent phase

The descent phase is described by a ballistic phase and a thermal/gliding phase.

The ballistic phase is a simple integration of the trajectory in a free-fall environment because air density is negligible. In order to do some computation, I integrated it until the spaceplane reach 100km altitude  so that i could begin the next phase.

The objective function is the maximisation of the distance travelled in order to reach an airport. 

First I tried to find some airports where the spaceplane could land, I filtered them so they have a long enough runway (around 2km) . I then inputed the position of the airport in the constraint.

![position of possible landing airports](img/landing_airports.png){ height=60% }


![Example of data visualised during a descent - State of vehicle](img/Fdescentstate.png){ width=80% }

![Example of data visualised during a descent - Aero and thermal](img/fdescentaerotherm.png){ width=80% }

\newpage


# Work done

My work has consisted mainly of doing several optimisations and analyse the data on why it works or not.

## Flow diagram

During the first week, in order to know how TROPICO works, I mapped all data flows inside the program to know exactly where each data goes.

See Diagram on Figure 3.

Thanks to this diagram, I managed to have a better idea on what input can be modified and what effect it can have.

This diagram also helped others lab interns like me to understand faster the TROPICO.

## New version of TROPICO

When I arrived in the LAB, a new version of TROPICO was being programmed. It is based on an older version where TROPICO is aimed to be more generalised program for MDO optimisation.

My job was to validate the new version of TROPICO. In order to do that, I computed the old input along with the first guesses and I tweaked options in order to get the same results as on the original paper.


## Trajectory visualisation

I created a script to get computed trajectories and visualise them into Google Earth. 

This can help visualising the trajectory in a 3D world.

Some examples :

![Google Earth Trajectory Examples with various descents from one ascent](img/earth.png)

\newpage
