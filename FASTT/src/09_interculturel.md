# Rapport culturel

Par consigne de Sigma Clermont, ce rapport culturel sera en français.

## Géographie

### Le Royaume-Uni

Le Royaume-Uni est un pays de l’Europe de l’Ouest doté d’une population de 65 millions d’habitants. C’est une monarchie constitutionnelle parlementaire dans laquelle Sa majesté la Reine Élisabeth II est reine depuis 1953. Le gouvernement actuel est présidé par Theresa May (Parti conservateur)

Le pays est composé de 4 principales nations constitutives : l’Angleterre, le Pays de Galles, l’Irlande du Nord et l’Écosse. 

Le pays fait partie de l’Union Européenne depuis 1973, mais les habitants ont voté par référendum la sortie de l’UE “Le Brexit” à 51,9%. Ce processus est censé duré deux ans et actuellement des négociations sont en cours sur les modalités de sortie avec les dirigeants européens.

### L’Écosse

L’Écosse était un état souverain jusqu’en 1707, date à laquelle le pays a été unifié avec l’Angleterre. Bien que sa capitale soit Edinburgh, la plus grande ville du pays est Glasgow, centre historique du développement industriel de la région. 

Les écossais sont marqués par un mouvement indépendantiste et depuis les élections législatives de 2011, le parti indépendantiste écossais a la majorité absolue au parlement écossais. En septembre 2014 a eu lieu un référendum portant sur l’indépendance de la région mais celui-ci a été rejeté à 55,3%.

L’Écosse est un pays possédant une culture très marquée par ces gens 

## Travail

### A l’université

Travailler dans un contexte de recherche était une nouveauté pour ma part. Le contact avec les autres doctorants fut très enrichissant car nous avions plusieurs points communs et nous avons pu partager des connaissances. De plus travailler sur MATLAB et particulièrement sur un sujet d’optimisation fut un pari assez risqué car ces compétences sont très peu abordés dans la filière SIL. 

Ce qui a été le plus destabilisant pour moi a été de travailler en presqu’autonomie et d’avoir très peu de consignes. Ceci m’a permis de développer et d’aiguiser mes compétences de programmation aussi bien MATLAB que un peu de Python.

Ce que j’ai apprécié lors de ce stage est que je pouvais faire les horaires qui me convenait. Ainsi j’ai pu adapter mes journées suivant les besoins et les temps de calcul sur l’ordinateur. Je pouvais également travailler depuis mon ordinateur personnel chez moi ce qui parfois m’évitait de me tremper sous le “soleil” écossais.

Un fait surprenant au premier abord dans le département Aerospace Engineering est que la majorité des doctorants et professeurs sont italiens. Il y a aussi beaucoup d’étrangers venant de nombreux pays et paradoxalement assez peu d’écossais ou anglais. Ainsi j’évoluais dans un milieu interculturel accueillant où chacun partager un peu de sa culture lors de discussions pendant les pauses. Ainsi contrairement à des amis qui travaillaient avec des britanniques, l’emploi du temps de tous les jours correspondaient à un rythme de vie franco/italiens et non écossais.

## Divertissements

### Les visites
Lors de ces 6 mois en Écosse, j’ai pu effectuer plusieurs visites autour de Glasgow. Ainsi j’ai découvert l’histoire de la région par ses chateaux comme Stirling [to add with photos]

![Maps of visit](img/visit.png)

### En pub

Les écossais adorent les pubs et la vie nocturne. En effet, il n’est pas rare de voir dès le vendredi 17h des pubs remplis. De plus, la ville de Glasgow est réputé pour avoir un pub à chaque coin de rue, ce qui est vrai !

![Carte des pubs dans Glasgow City center](img/pub.png)

La culture de l’alcool est très différente par rapport en France. En effet, nous sommes élevés à boire de l’alcool avec modération. En Écosse, il y a un certain problème d’alcoolisme car il n’est pas rare de voir dans le journal local, des problêmes ou des campagnes luttant contre ce fléau.

### La nourriture

La nourriture en Écosse est assez spéciale pour un français.
D'une part il y a la traditionnelle cuisine de pub 

En effet, il y a tout d'abord la traditionnelle cuisine de pub où on peut trouver généralement des fish and chips, des frites aux cheddars fondus, et tout autres classiques de la nourriture anglo-saxonne. 

Le haggis est aussi le plat typique en Écosse, c'est tout simplement de la panse de brebis farçie, et cela est très bon!

Dans les _chippies_ locaux, on trouve également des choses assez inhabituelles. Par exemple, beaucoup de fast food proposent de la nourriture frite, tel la barre de chocolat Mars frite dans l'huile. Avec cela, on attrape un an de cholestérol en une bouchée mais le goût est présent! La légende dit même que l'on peut venir avec sa propre nourriture et la faire frire dans un chippy.

### Les concerts

Glasgow est une ville ultra musicale. En effet de grands groupes de musiques passent par les différentes salles existantes dans Glasgow. De plus la scène glaswegian est très fournie car le nombre de pubs possédant une petite salle de concert est énorme. 

Pour ma part, j'ai pu aller écouter un concert par semaine. Les groupes sont tellement riches, que l'on peut décider même du genre de musique que l'on veut voir presque chaque semaine.

Ainsi de nombreux lieux sont mythiques comme le Kings Tut Wah Wah hut, le Barrowland, ou le SSE Hydro.

Il y a également la possibilité si on est musicien de participer à des boeufs musicaux chaque semaine avec un thème différent. (blues, funk, rock, jazz...)

## Choc culturel

### TAPS AFF et autres “écosseries”

Le premier choc culturel s'est fait sur le climat local. En effet j'ai été en Écosse du mois de Mars au mois d'Aout, et la preùière chose frappante et le peu de variance dans la température. Les journées d'été étaient comprises entre 13/14 à 16/17°C suivant si c'était un jour de beau temps ou non. 

De ce fait, les écossais sont très sensibles à la chaleur et j'ai vécu deux semaines de canicule écossaisse où il a fait 22 à 24°C. Les écossais pratiquent alors le "TAPS AFF SUN'S OOT" qui est simplement le fait qu'ils se baladent souvent torse nu en plein centre ville de Glasgow. C'est assez perturbant car en tant que français nous sommes habitués à ce climat.

Une autre chose étonnante est le faible habillement de la gente féminine en soirée où les mini-jupes et haut courts sont de sortie la nuit pour les soirées en discothèque alors qu'il fait froid (de l'ordre de 6 à 10°C) et que nous sommes en manteau chaud. L'important est quand même qu'elle possède un parapluie pour se protéger de la pluie intempestive écossaisse.


\newpage
