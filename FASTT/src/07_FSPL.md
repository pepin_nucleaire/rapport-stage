# The Project

## Context
### Commercial opportunities

The forecast market of small satellites launches such as cubesat is rising exponentially in recent market analysis. This is why Orbital Access, a private company based in Prestwick, Scotand, is leading a private & public consortium of various companies and universities to make a reusable UK-made launcher to secure the market demand. This work is funded by the UK Space Agency.

![Baseline Market forecast](img/baseline.png){ width=65% }

## Vehicle requirements definition

After the market analysis, the consortium established requirements for the vehicle: 

* Air Launch utilising modified DC10 / MD11 carrier aircraft platform.
* Re-usable fly-back first stage.
* Nominal mission, 500 kg payload to 650 km circular LEO at 88.2 deg inclination (3 x OneWeb satellites to OneWeb parking orbit) (Selding, 2014)
* Extended mission, 150 kg payload to 1200 km circular LEO at 88.2 deg inclination (OneWeb operational orbit) (Selding, 2014)
* Reference launch location- carrier aircraft takeoff from Prestwick with air drop location near west of Hebrides
* Price target $30,000 per kg

### Vehicle configuration

These requirements drove the consortium to find a viable vehicle for a commercial-purposed launcher. 

The vehicle studied got this configuration : 

* One piece delta wing or delta with LE strakes (stepped delta)
* Integral payload bay for upper stages and payloads (parallel multiple stages if required)
* Peripheral tanks arranged singly or in parallel to optimise CG travel during propellant burn. Conformal RP1 tanks
* In flight propellant loading for booster and upper stages
* Emergency propellant jettison
* Folding dorsal and ventral fins for carriage and release
* Light weight undercarriage sized for landing booster c/w upper stage and payload (flight abort configuration)
* Fully autonomous flight from release to landing

![Preliminary design FSPL](img/fspl_predesign.png){ width=40% }

This vehicle is called the FSPL. A two-stage to orbit 



### Software
The main software used during this project was TROPICO, a MATLAB-written program created within the FASTT Lab made by Federico Toso, a PhD student and Christie Maddock. 

TROPICO is a specialised platform used to analyse the performance and optimise a mission design for transatmospheric flight vehicles. 

![Architecture of MATLAB program](img/architecture.png)

For each simulation, input models are entered into a mission file. Settings specified for the whole optimisation are :

* Constraints on acceleration, temperature, and dynamic pressure
* First guess generation settings
* Objective function 
* Scaling settings
* A no-fly zone

Afterwards, the mission is designed with several phases that are defined by different models and constraints such as which vehicle is chosen and what models are applied to it, how many control points for the phase, what are the bounds and some settings as what integration and interpolation algorithm is chosen.

\newpage
