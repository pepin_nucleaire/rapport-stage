# Acknowledgements {.unnumbered}

<!-- This is for acknowledging all of the people who helped out -->

I want to thank Dr. Christie Maddock for accepting me and helping me understanding a lot of optimisation concepts that I didn't know before.

Also, I want to thank Federico Toso, a PhD student working on TROPICO for helping me on the FSPL problem and on tackling various bugs encountered.

Thanks to Mrs Audrey Marguerie for helping me to find this internship at the last moment because I had an issue finding a placement just before coming to Strathclyde.

<!-- Use the \newpage command to force a new page -->

\newpage

\tableofcontents

\newpage
