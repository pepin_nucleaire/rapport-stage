# Internship context

The goal of this international year is to work with other cultures and meet people with differents perspectives and personalities that we get used to in France engineering schools and work spaces. This is why I took a gap year doing two semesters in two different countries during the middle of my last year of studies. 

This report is a resume and review of my first period I spent at the University of Strathclyde in Glasgow, Scotland.

Going to Scotland was a last minute opportunity because I planned to go to Chile, however this plan was canceled and I had to find a new internship. 
I want to thank both my tutors Dr Christie Maddock and Audrey Marguerie because they helped me to have this placement and taught me lots of new knowledge.

This report will first present the University of Strathclyde and the FASTT Lab, then I will present my work done there and I will finish with a cultural report about Scotland.


\newpage


