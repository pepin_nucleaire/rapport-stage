# Conclusion

Grâce à cette expérience de travail académique en Écosse, j'ai pu découvrir de nombreux aspects de la vie Écossaisse ainsi que le fonctionnement d'un laboratoire de recherche académique.

Je me suis rendu compte que malgré le sujet intéressant, le monde académique n'est pas dans mon projet professionnel car il y a beaucoup de frustrations apportées du fait du nombre d'hypothèses qui n'aboutissent peu ou pas à un résultat concret et utilisable. De plus j'ai pu voir par mes collègues doctorants qu'il existe une pression pour écrire des documents scientifiques rapidement, parfois en étant moins rigoureux sur l'obtention des résultats.

Mais d'un point de vue personnel, j'ai fortement apprécié la vie écossaisse que ce soit par les paysages que le pays possède, ou par les gens qui sont très humbles et faciles à vivre. Ils sont également généreux et il est facile de les approcher et discuter avec eux.

En conclusion, ce semestre d'année international fût très enrichissant et je recommande aux futures stagiaires de le faire ici en Écosse.