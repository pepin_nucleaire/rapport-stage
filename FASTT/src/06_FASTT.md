# University of Strathclyde

## The university

The University of Strathclyde was founded in 1796 as the Andersonian Institute. It is a public research university located in the city centre of Glasgow in Scotland. There are around 22,000 students (undergraduate & postgraduate levels) and 3000 administrative staffs.

The University ranks among the top 30 best research universities in United Kingdom and many of its various departments are high ranked among the country.

## FASTT Lab
The Centre for Future Air-Space Transportation Technology (cFASTT) is a research centre at the University of Strathclyde that was established in 2010 with the intent of revolutionising future air and space travel.

Its main areas of research are :
 Aerodynamics
 Thermodynamics and thermo-regulation
 Propulsion and structures
 Flight dynamics and trajectory management
 Multidisciplinary design optimisation
 Regulatory and operational aspects
 Physiological issues
 Environmental impact
 Societal impact

\newpage