<!-- 
This is the Latex-heavy title page. 
People outside UCL may want to remove the header logo 
and add the centred logo
-->

\begin{titlepage}
    \begin{center}

    % Delete the following line
    % to remove the UCL header logo
    \includegraphics[width=5cm]{img/sigma.png} \includegraphics[width=5cm]{img/strath.png} 
    
        \vspace*{2.5cm}
        
        \huge
        Multidisciplinary design optimisation of a multi-stage-to-orbit spaceplane
        
        \vspace{1.5cm}
        
        \Large
        Julien Muller

        \vspace{1.5cm}

        \normalsize
        Internship report \\
        International year S1
        
        \vfill
        
        \normalsize
        Sigma tutor : Audrey Marguerie  \\
        Strathclyde tutor : Dr. Christie Maddock

        \vspace{0.8cm}

        % Uncomment the following line
        % to add a centered university logo
        % \includegraphics[width=0.4\textwidth]{style/univ_logo.eps}
        
        \normalsize
        University of Strathclyde\\
        From March 6th to September 1st 2017
        

        % Except where otherwise noted, content in this thesis is licensed under a Creative Commons Attribution 4.0 License (http://creativecommons.org/licenses/by/4.0), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited. Copyright 2015,Tom Pollard.

    \end{center}
\end{titlepage}
